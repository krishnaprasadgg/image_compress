from django.shortcuts import render, render_to_response, RequestContext
from img.forms import TImages1
from img.models import TImages

# Create your views here.

def home(request):
    form=TImages1(request.POST or None)
    return render_to_response('home.html',locals(),context_instance=RequestContext(request))

def strit(request):
    form=TImages1(request.POST or None)
    if form.is_valid():
        save_img = form.save(commit=False)
        save_img.save()
    return render_to_response('home.html',locals(),context_instance=RequestContext(request))
from django.db import models
from PIL import Image
# Create your models here.

class TImages(models.Model):
    title      = models.CharField(max_length = "200")
    #     image      = models.FileField(upload_to='property_images')
    image = models.ImageField(upload_to="ptest",null=True,blank=True,editable=True)
    image_height = models.PositiveIntegerField(null=True, blank=True, editable=False, default="100")
    image_width = models.PositiveIntegerField(null=True, blank=True, editable=False, default="100")

    def save(self):
        if not self.image:
            return "{0}".format(self.image)

        super(TImages, self).save()
        image = Image.open(self.image)
        (width, height) = image.size
        size = ( 450, 300)
        image = image.resize(size, Image.ANTIALIAS)
        image.save(self.image.path)


    def __unicode__(self):
        return self.title